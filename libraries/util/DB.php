<?php

class DB{
   // atributos 

   /**Endereço do site que hospeda o banco de dados */
   private $host;

   /**Nome do usuário */
   private $user;

   /** Senha de acesso ao bd */
   private $pass;

   /** Nome do banco de dados */
   private $name;
   
   private $link;

   // construtor: inicializa os atributos da class
   function __construct(){
       $this->host = "localhost";
       $this->user = "root";
       $this->senha = "";
       $this->name = "loo";
       $this->link = new mysqli($this->host, $this->user, $this->pass, $this->name);
   }

   public function get($stable){
      $SQL = "SELECT * FROM $stable";
      return $this->query($sql);
   }

   public function query($sql){
      $rs = $this->link->query($sql);
      
      $data = array();
      while ($row = $rs->fetch_assoc()){
        $data[] = $row;
      }   
      return $data;
   }

   /**
    * Insere data na tabela table do banco de dados
    * @param string $table: o nome da tabela
    * @param array $data: vetor associativo
    * @return int: id do registro inserido no bd
    */
   public function insert($table, $data){
      $keys = array_keys($data);
      $colunas = implode(', ', $keys);

      $values = array_values($data);
      $valores = implode("', '", $values);

      $sql = "INSERT INTO $table ($colunas) VALUES ('$valores')";
      $rs = $this->link->query($sql);
      echo $this->link->error;

      return $this->link->insert_id;
   }
}