<?php

class Pessoa {
    // atributos
    private $nome;
    private $peso;
    private $idade;
    private $altura;

    // construtor
    function __construct($nome){
        $this->nome = $nome;
    }

    // métodos
    public function dormir(){
        echo 'Descanso de, pelo menos 12 horas.<br>';
    }

    public function comer($comida){
        echo "Eu gosto de comer $comida<br>";
    }

    public function falar($assunto, $interlocutor = "eu mesmo"){
        echo "Costumo falar sobre $assunto com $interlocutor<br>";
    }

    // métodos acessores
    public function getNome(){
        return $this->nome;
    }
}