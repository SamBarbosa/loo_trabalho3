<?php
include_once 'Pessoa.php';

class Professor extends Pessoa {
    private $especialidade;
    private $escola;

    // sobrescrita de método da classe pai
    function __construct($nome, $escola){
        parent::__construct($nome);
        $this->escola = $escola;
    }

    public function pesquisar($tema){
        echo "Passar horas na Internet buscando $tema<br>";
    }

    public function lecionar($tema, $disciplina){
        echo "Todo dia estudo $tema da disciplina $disciplina";
    }

    public function getNome(){
        return "Professor ".parent::getNome();
    }
}