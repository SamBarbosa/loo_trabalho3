<header>
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
    <div class="container">
      <a class="navbar-brand" href="#">
        <strong>UPA</strong>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
        aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?= BASEURL ?>index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= BASEURL ?>upload/index.php">Upload</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= BASEURL ?>upload/lista.php">Lista dos Arquivos Enviados</a>
          </li>
        </ul>
        <form class="form-inline">
          <div class="md-form my-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Buscar">
          </div>
        </form>
      </div>
    </div>
  </nav>
  <div class="view jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/76.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <div class="mask rgba-white-light d-flex justify-content-center align-items-center">
      <div class="container">
        <div class="row">
          <div class="col-md-12 white-text text-center">
            <h1 class="display-3 mb-0 pt-md-5 pt-5 white-text font-weight-bold wow fadeInDown" data-wow-delay="0.3s">UPLOAD 
              <a class="indigo-text font-weight-bold">DE ARQUIVOS</a>
            </h1>
            <h5 class="text-uppercase pt-md-5 pt-sm-2 pt-5 pb-md-5 pb-sm-3 pb-5 white-text font-weight-bold wow fadeInDown"
              data-wow-delay="0.3s"></h5>
            <div class="wow fadeInDown" data-wow-delay="0.3s">
              <a class="btn btn-light-blue btn-lg" onclick="alert('Nome: Samela Barbosa Claudino, GU:1800264, Idade: 20, Data de Nascimento: 10/05/1999, Escolaridade: Ensino Fundamental e Ensino Médio Completo, Curso: Informática Para Internet com duração de 4 semestres')">Feito Por:</a>
              <a class="btn btn-indigo btn-lg" onclick="alert('Sistema desenvolvido com o intuito para o envio de arquivos ')">Site</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>