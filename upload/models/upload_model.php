<?php
include_once APPPATH.'libraries/componente/Tabela.php';
include 'libraries/UploadManager.php';

if(sizeof($_POST) > 0){
    $upload = new UploadManager();
    $data['nome'] = $_POST['nome'];
    $data['descricao'] = $_POST['descricao'];
    

    if($upload->done($data)){
        echo 'Arquivo enviado com sucesso';
    }
    else{
        echo 'Falha no envio do arquivo';
    }
}

function lista_arquivos(){
    $upload = new UploadManager();
    $lista = $upload->lista_arquivos();
    $label = array('Nº', 'Nome', 'Descrição', '');

    $tabela = new Tabela($lista, $label);
    return $tabela->getHTML();
}


function remove_arquivo($id){
    $upload = new UploadManager();
    if($upload->remove($id)){
        redirect('location: http://localhost/upload/lista.php');
    }
    else{
        //exibir pagina com mensagem de erro
    }
}
?>