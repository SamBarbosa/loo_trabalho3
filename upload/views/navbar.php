<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark info-color #33b5e5">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="#">UPA</a>

  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= BASEURL ?>index.php">Home
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASEURL ?>upload/index.php">Upload</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASEURL ?>upload/lista.php">Lista dos Arquivos Enviados</a>
      </li>

    </ul>
    <!-- Links -->

    <form class="form-inline">
      <div class="md-form my-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Buscar">
      </div>
    </form>
  </div>
  <!-- Collapsible content -->


</nav>
<!--/.Navbar-->