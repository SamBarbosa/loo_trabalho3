<?php

class Table {
    // atributos
    private $colunas;
    private $conteudo;
    private $tableClasses = '';
    private $headerClasses = '';

    // construtor
    function __construct($colunas,$conteudo){
        $this->conteudo = $conteudo;
        $this->colunas = $colunas;
    }

    private function header(){
        $html = ' <thead class="'.$this->headerClasses.'"><tr>';
        foreach ($this->colunas AS $coluna){
            $html .= '<th scope="col">'.$coluna.'</th>';
        }
        $html .= ' <tr></thead>';
        return $html;
    }

    public function addHeaderClasses($class){
        $this->headerClasses .= "$class ";
    }

    public function addTableClass($class){
        $this->tableClasses .= "$class ";
    }

    private function body(){
        $html ="<tbody>";
        foreach ($this->conteudo AS $linha){
            $html .= '<tr>';
            foreach ($linha AS $coluna){
                $html .= '<td scope="col">'.$coluna.'</td>';
            }
            $html .= '</tr>';
        }
        $html .="</tbody>";
        return $html;
    }

    // métodos
    public function getHTML(){
        $html = '<table class="table '.$this->tableClasses.'">';
        $html .= $this->header();
        $html .= $this->body();
        $html .= '</table>';
        return $html;
    }
}