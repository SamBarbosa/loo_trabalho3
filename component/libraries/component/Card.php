<?php

class Card{
	// declara��o de atributos dos objetos da classe
	private $imagem;
	private $titulo;
	private $conteudo;
	private $rotulo_botao;
	
	// m�todo construtor: inicializa os atributos da classe
	function __construct($titulo, $conteudo, $imagem, $rotulo_botao){
		$this->titulo = $titulo;
		$this->conteudo = $conteudo;
		$this->imagem = $imagem;
		$this->rotulo_botao = $rotulo_botao;	
	}	
	
	public function getHTML(){
		$html= '	
			<div class="card col-md-4">
				<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/'.$this->imagem.'.jpg" alt="Card image cap">
				<div class="card-body">
					<h4 class="card-title"><a>'.$this->titulo.'</a></h4>
					<p class="card-text">'.$this->conteudo.'.</p>
					<a href="#" class="btn btn-primary">'.$this->rotulo_botao.'</a>
				</div>
			</div>
		';
		return $html;
	}
}