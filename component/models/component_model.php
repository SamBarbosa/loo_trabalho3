<?php
include APPPATH.'component/libraries/component/Card.php';
include APPPATH.'component/libraries/component/table.php';

// model -> produzir os dados que ser�o enviados na tela
// geraçao de dados e operaçoes logicas

function get_cards(){
	$db = new DB();
	$v = $db->get('card');
	$html = '';
	
	foreach($v AS $data){
		$titulo = $data['title'];
		$conteudo = $data['conteudo'];
		$imagem = $data['imagem'];
		$rotulo_botao = $data['rotulo_botao'];
		$card = new Card($titulo, $conteudo, $imagem, $rotulo_botao);
		$html .= $card->getHTML();
		
	}
	
	return $html;
}

function get_table(){
	$cols = array('Nome', 'Sobrenome', 'E-mail', 'Senha', 'Telefone');
	$rows = array(
		array('nome'=>'Diego', 'sobrenome'=>'Silva', 'email'=>'user@gmail.com', 'senha'=>'123456', 'telefone'=>'2569-8745'),
		array('nome'=>'Maria', 'sobrenome'=>'Santos', 'email'=>'user@gmail.com', 'senha'=>'123456', 'telefone'=>'2569-8745'),
		array('nome'=>'Luana', 'sobrenome'=>'Silva', 'email'=>'user@gmail.com', 'senha'=>'123456', 'telefone'=>'2569-8745'),
	);

	$table = new Table($cols,$rows);
	$table->addHeaderClasses('secondary-color-dark white-text');
	$table->addTableClass('table-striped table-bordered ');
	return $table->getHTML();
}


