-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 10-Jun-2019 às 22:25
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `dados_pessoais`
--

CREATE TABLE `dados_pessoais` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `sobrenome` varchar(80) NOT NULL,
  `nascimento` varchar(10) NOT NULL,
  `estado_civil` varchar(20) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dados_pessoais`
--

INSERT INTO `dados_pessoais` (`id`, `nome`, `sobrenome`, `nascimento`, `estado_civil`, `deleted`, `last_modified`) VALUES
(1, 'Jose', 'Silva', '20/02/2002', 'casado', 0, '2019-05-20 19:24:52'),
(2, 'Jose', 'Silva', '20/02/2002', 'casado', 0, '2019-05-20 19:34:39'),
(3, 'Jose', 'Silva', '10/05/1968', 'casado', 0, '2019-05-20 19:35:05'),
(4, 'samela', 'barbosa', '10/05/1999', 'solteira', 0, '2019-05-20 20:48:08'),
(5, 'Isabela', 'Barbosa', '10/08/1999', 'Solteira', 0, '2019-06-10 19:27:24');

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefone`
--

CREATE TABLE `telefone` (
  `id` int(11) NOT NULL,
  `comercial` varchar(16) NOT NULL,
  `pessoal` varchar(16) NOT NULL,
  `residencial` varchar(16) NOT NULL,
  `id_dados_pessoais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `telefone`
--

INSERT INTO `telefone` (`id`, `comercial`, `pessoal`, `residencial`, `id_dados_pessoais`) VALUES
(2, '20569655', '', '', 0),
(3, '20569655', '', '', 0),
(4, '20569655', '', '', 0),
(5, '20569655', '', '', 0),
(6, '20569655', '', '', 0),
(7, '20125698', '20215658', '52362569', 4),
(8, '11 20568958', '11 20369669', '11 20147898', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `upload`
--

CREATE TABLE `upload` (
  `id` int(11) NOT NULL,
  `nome` varchar(128) NOT NULL,
  `arquivo` varchar(64) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `id_categoria` tinyint(3) UNSIGNED NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `upload`
--

INSERT INTO `upload` (`id`, `nome`, `arquivo`, `descricao`, `id_categoria`, `deleted`, `created_at`) VALUES
(1, 'data', '', 'arquivo', 0, 0, '2019-06-10 18:33:41'),
(2, 'Arquivo', '', 'Arquivo', 0, 0, '2019-06-10 18:37:13'),
(3, 'arquivo', '', 'arquivo', 0, 0, '2019-06-10 18:44:06'),
(4, 'arquivo', '', 'arquivo', 0, 0, '2019-06-10 18:46:45'),
(5, 'arquivo', '', 'arquivo', 0, 0, '2019-06-10 18:46:46'),
(6, 'arquivo', '', 'arquivo', 0, 0, '2019-06-10 18:46:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `sobrenome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(512) NOT NULL,
  `telefone` varchar(18) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `sobrenome`, `email`, `senha`, `telefone`, `last_modified`) VALUES
(1, 'Samela', 'Barbosa', 'samela@gmail.com', '123456', '11 25636955', '2019-03-11 19:43:50'),
(2, 'Isadora', 'Prado', 'isadora@gmail.com', '123456', '11 25478100', '2019-03-11 19:44:01'),
(3, 'Vitor', 'Oliveira', 'vitor@gmail.com', '123456', '11 52697841', '2019-03-11 19:44:05'),
(4, 'Samuel', 'Barbosa', 'samuel@gmail.com', '123456', '11 45213359', '2019-03-11 19:44:39'),
(5, 'Leticia', 'Silva', 'leticia@gmail.com', '123456', '11 20695124', '2019-03-11 19:52:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dados_pessoais`
--
ALTER TABLE `dados_pessoais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telefone`
--
ALTER TABLE `telefone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dados_pessoais`
--
ALTER TABLE `dados_pessoais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `telefone`
--
ALTER TABLE `telefone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
